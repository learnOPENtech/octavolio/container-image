FROM debian:bullseye-slim as source
ADD https://launchpadlibrarian.net/607355324/mahara-22.04.2.tar.gz /
RUN tar -zxf mahara-22.04.2.tar.gz

FROM php:7.4-apache-bullseye as build
RUN apt-get update; apt-get install -y libpq-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    zlib1g-dev \
    libzip-dev \
    libicu-dev \
    ; apt-get clean
RUN ldconfig; pkg-config --list-all
RUN docker-php-ext-configure gd --with-freetype --with-jpeg && docker-php-ext-install gd intl pgsql zip
RUN yes '' | pecl install redis && docker-php-ext-enable redis

FROM php:7.4-apache-bullseye
RUN apt-get update; apt-get install -y libfreetype6 \
    intltool \
    libjpeg62-turbo \
    libpq5 \
    libpng16-16 \
    libzip4 \
    ; apt-get clean
COPY --from=source /mahara-22.04.2/htdocs /var/www/html
COPY --from=build /usr/local /usr/local
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN echo 'post_max_size = 100M' > "$PHP_INI_DIR/conf.d/post-max-size.ini"
WORKDIR /var/www/html
RUN mv config-environment.php config.php
RUN mkdir -p /mahara/data && chown www-data:www-data /mahara/data
VOLUME /mahara/data
EXPOSE 80
